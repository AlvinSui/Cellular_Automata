#! /usr/bin/env python

"""
    A few algorithms for curve fitting, first and second derivatives and
    integrals using numpy.
"""

import matplotlib.pyplot as plt
import numpy as np
import csv

# read data from a csv file and convert it to floats
unemployment = []
with open("Unemployment_isolationism 25 years.csv", 'r') as curFile:
    curFileReader = csv.reader(curFile, delimiter=',')
    # first line is bogus so skip
    curFileReader.next()
    # read just one line and discard the first element
    row = curFileReader.next()[1:]
    # convert strings to float using numpy function asfarray
    unemployment = np.asfarray(row, float)

# create a polynomial (curve) of the input data
t = np.linspace(0, 26)
x = np.arange(0, 26)

#years = np.array(np.arange(t))
#unemp_curve = np.polyfit(years, unemployment, 30)
#c = np.poly1d(unemp_curve)

# first and second derivative

# integral

# curve fitting

# print graphs
#plt.plot(years, c(years))
#plt.scatter(x, unemployment)
#plt.show()

# print some statistical data
#print "The median unemployment rate is: %f" % np.median(unemployment)
#print "The weighted average unemployment is %f" % np.average(unemployment)
#print "The variance in unemployment is %f" % np.var(unemployment)

#
# Interpolation function
# Newton's Divided Difference
# see "Numerical Methods in Engineering" page 120
#
def divided_difference(x, y, order):
    if order > len(x) - 1:
        print "Error, not enough sample points"
        return []
    coefficients = [y[0]]
    for i in range(1, order + 1):
        print "Calculating %d order" % i
        fx = (y[i] - y[i-1]) / (x[i] - x[i-1])
        coefficients.append((fx - coefficients[i-1]) / (x[i] - x[0]))

    return coefficients


def divided_sample_curve(coefficients, x, position):
    result = coefficients[0]
    coefficients = coefficients[1:]
    mult   = 1.0
    for i, c in enumerate(coefficients):
        mult   *= (position - x[i])
        result += c * mult

    return result

# data from example 4.2 p. 122
x = [2.0, 3.0, 4.0]
y = [0.2239, -0.2601, -0.3971]
coeff = divided_difference(x, y, 2)
if len(coeff) != 0:
    sample = divided_sample_curve(coeff, x, 3.2)
print "Polynomial coefficients: %s" % coeff
print "Sample returns: %f" % sample
