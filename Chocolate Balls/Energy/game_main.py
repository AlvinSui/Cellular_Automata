"""
BZZZ ZZZy  ZZZZZZZ  ,ZZZZZZZ  ZZZB ZZZ    ZZZZZz    ZZZ,ZZZj  ZZZZ ZZZ       ZZZ BZZZ  DZZZ,  ZZZZ  ZZZZ
 ZZ   Z5    Zj w 5   wZ   j,   zZ  ZW    ZZ   yZE   DZw  Z9    Z Zw ZW       Zy ZZ     9Z     Z,Zj Z Z
 ZZZZZZz    ZZZZ     jZZZz      8ZZD     ZZ    ZZ   zZj  ZZ    Z 8Z Zy       ZZZZ      ZZ     Z EZ Z Z
 ZZ   ZD    Zy  DZ   WZ   ZB     ZZ      ZZj  ZZD   8ZD  ZZ    Z  Z8Z5       ZD 8Zy    EZ     Z  ZZj Z
8ZZZ ZZZD  ZZZZZZZ  WZZZZZZ9    ZZZZ      jZZZZ      8ZZZZ    ZZZ wZZz       ZZZy9ZZZ  zZZZw  ZZZ ZZ ZZZ

20180606 @Heeyoun Kim
2018 Why Factory_Future Model_Team Energy

######### TEST PYTHON CODES #########

## This class will work as overal controler in the game.
## 1. turn start
## 2. return_data : return datas to the internal database.
## 3.

#turn start
#return_datas to the internal database
"""


from distribution_Controller import distribution_Controller as dc
from parameter_Controller import parameter_Controller as pc
#from triangle_selector import triangle_selector as trisel
#from turn_system import turn_system as ts
from calc_Engine import calc_energygame as ce
print('NOTICE : import modules')

##################################################################
################ S A M P L E    D I C T I O N R Y ################
##################################################################

# turn_test variable
triangle_a = {'land': 1, 'mountain': 0, 'urban': 0, 'population': 39843100, 'population_default': 39843100,
                  'temperature': 20.79, 'temp_delta': 0.9,
                  'energy_demands': 10, 'pollution': 10, 'energy_satisfaction': 1, 'transition_value': 0.1,
                  'oil_reserves': 63689300, 'oil_production': 5547860,
                  'coal_reserves': 1176220000, 'coal_production': 2653140,
                  'gas_reserves': 52601100, 'gas_production': 5600540,
                  'solar_potential': 2, 'rain_potential': 0, 'wind_potential': 0, 'water_potential': 0,
                  'total_potential': 2
                  }

triangle_b = {'land': 1, 'mountain': 1, 'urban': 1, 'population': 1747450, 'population_default': 1747450,
                  'temperature': 37.6629, 'temp_delta': 0.9,
                  'energy_demands': 1, 'pollution': 10, 'energy_satisfaction': 1, 'transition_value': 0.001,
                  'oil_reserves': 31359500, 'oil_production': 334114,
                  'coal_reserves': 6658130, 'coal_production': 39781,
                  'gas_reserves': 2303960, 'gas_production': 220710,
                  'solar_potential': 2, 'rain_potential': 0, 'wind_potential': 0, 'water_potential': 0,
                  'total_potential': 2
                  }


def start_turn(value1):

    turn_in = value1

    print("NOTICE : game is starting")
    #turn_in = input("Turn NO: ")

    print("NOTICE: turn: ", turn_in)

    while turn_in <= 101:

        if turn_in > 100:
            print("GAME HAS ALREADY FINISHED! GO HOME!!!!")
            break

        elif turn_in <= 100:
            engine = ce()
            print("else is starting")

            """
            a = ts(turn_in)
            a.turn_counter()
            a.phase_counter()
            a.turn_max_checker()
            a.gameoverCheker()

            tri_selector = trisel()

            ### USER INPUT PART ###
            first_triangle = input("please select triangle 1-320: ")
            second_triangle = input("please select triangle 1-320: ")

            tri_selector.tri_setter_first(first_triangle)
            tri_selector.tri_setter_second(second_triangle)
            print(tri_selector.first_triangle)
            print(tri_selector.second_triangle)
            """

            oil_production = input("how much oil will you produce this turn?: ")
            coal_production = input("how much coal will you produce this turn?: ")
            gas_production = input("how much gas will you produce this turn?: ")

            b = pc()
            b.oil_getter(oil_production)
            b.coal_getter(coal_production)
            b.gas_getter(gas_production)

            print("You product oil: ", b.inputvalue_oil)
            print("You product coal: ", b.inputvalue_coal)
            print("You product Gas: ", b.inputvalue_gas)

            """
            ship_distribution = raw_input("Do you want to use ship? on or off:")
            truck_distribution = raw_input("Do you want to use truck? on or off:")
            train_distribution = raw_input("Do you want to use train? on or off:")
            grid_distribution = raw_input("Do you want to use grid? on or off:")

            c = dc()
            c.ship_selector(ship_distribution)
            c.truck_selector(truck_distribution)
            c.train_selector(train_distribution)
            c.grid_selector(grid_distribution)
            """

            dist = dc()


            ## ALGOLITHM PART ##
            population_a = triangle_a["population"]
            population_b = triangle_b["population"]

            population_a = engine.population_calc(population_a, turn_in, triangle_a['pollution'])
            population_b = engine.population_calc(population_b, turn_in, triangle_b['pollution'])


            temperature_a = triangle_a['temperature']
            temperature_b = triangle_b['temperature']

            energy_capita_a = engine.en_calc(temperature_a)
            energy_capita_b = engine.en_calc(temperature_b)

            energy_demand_a = engine.ed_calc(energy_capita_a, population_a)
            energy_demand_b = engine.ed_calc(energy_capita_b, population_b)

            renewable_demand_a = engine.rd_calc(energy_demand_a, triangle_a['transition_value'])
            renewable_demand_b = engine.rd_calc(energy_demand_b, triangle_b['transition_value'])

            ### FOSSIL A ###

            fossil_demand_a = engine.fd_calc(energy_demand_a, renewable_demand_a)
            fossil_demand_b = engine.fd_calc(energy_demand_b, renewable_demand_b)

            oil_prod_a = engine.oil_prod(triangle_a['oil_reserves'], fossil_demand_a, oil_production)
            oil_reserve_a = engine.oil_reserve(triangle_a['oil_reserves'],oil_prod_a)

            coal_prod_a = engine.coal_prod(triangle_a['coal_reserves'], fossil_demand_a, coal_production)
            coal_reserve_a = engine.coal_reserve(triangle_a['coal_reserves'], coal_prod_a)

            gas_prod_a = engine.gas_prod(triangle_a['gas_reserves'], fossil_demand_a, gas_production)
            gas_reserve_a = engine.gas_reserve(triangle_a['gas_reserves'], gas_prod_a)

            fossil_prod_a= engine.fossil_prod(oil_prod_a, coal_prod_a, gas_prod_a)

            ### FOSSIL B ###

            oil_prod_b = engine.oil_prod(triangle_b['oil_reserves'], fossil_demand_b, 0)
            oil_reserve_b = engine.oil_reserve(triangle_b['oil_reserves'], oil_prod_b)

            coal_prod_b = engine.coal_prod(triangle_b['oil_reserves'], fossil_demand_b, 0)
            coal_reserve_b = engine.coal_reserve(triangle_b['coal_reserves'], coal_prod_b)

            gas_prod_b= engine.gas_prod(triangle_b['gas_reserves'], fossil_demand_b, 0)
            gas_reserve_b = engine.gas_reserve(triangle_b['gas_reserves'], gas_prod_b)

            fossil_prod_b = engine.fossil_prod(oil_prod_b, coal_prod_b, gas_prod_b)

            #RENEWABLE
            renew_solar_a = triangle_a['solar_potential']
            renew_rain_a = triangle_a['rain_potential']
            renew_wind_a = triangle_a['wind_potential']
            renew_water_a = triangle_a['water_potential']
            renew_total_a = triangle_a['total_potential']

            renew_solar_b = triangle_a['solar_potential']
            renew_rain_b = triangle_a['rain_potential']
            renew_wind_b = triangle_a['wind_potential']
            renew_water_b = triangle_a['water_potential']
            renew_total_b = triangle_a['total_potential']

            solar_prod_a = engine.renew_solar_prod(renew_solar_a, renew_total_a, renewable_demand_a)
            rain_prod_a = engine.renew_rain_prod(renew_rain_a, renew_total_a, renewable_demand_a)
            wind_prod_a = engine.renew_wind_prod(renew_wind_a, renew_total_a, renewable_demand_a)
            water_prod_a = engine.renew_water_prod(renew_water_a, renew_total_a, renewable_demand_a)
            renew_total_prod_a = engine.renew_total_prod(solar_prod_a, rain_prod_a, wind_prod_a, water_prod_a)

            solar_prod_b = engine.renew_solar_prod(renew_solar_b, renew_total_b, renewable_demand_b)
            rain_prod_b = engine.renew_rain_prod(renew_rain_b, renew_total_b, renewable_demand_b)
            wind_prod_b = engine.renew_wind_prod(renew_wind_b, renew_total_b, renewable_demand_b)
            water_prod_b = engine.renew_water_prod(renew_water_b, renew_total_b, renewable_demand_b)
            renew_total_prod_b = engine.renew_total_prod(solar_prod_b, rain_prod_b, wind_prod_b, water_prod_b)

            #OVERALL
            overall_prod_a = engine.overall_production(fossil_prod_a, renew_total_prod_a)
            overall_prod_b = engine.overall_production(fossil_prod_b, renew_total_prod_b)

            #DELTA
            delta_a = engine.delta_calc(energy_demand_a, overall_prod_a)
            delta_b = engine.delta_calc(energy_demand_b, overall_prod_b)
            delta_check = engine.delta_checker(delta_a, delta_b)

            #DISTRIBUTION (FAKE IN THE PYTHON)
            distribution = dist.dist_checker(3, 10, delta_check)

            #ENERGY SUPPLY
            energy_supply_a = engine.energy_supply_sketch3(distribution, overall_prod_a, energy_demand_a, delta_a, delta_check)
            energy_supply_b = engine.energy_supply_sketch3(distribution, overall_prod_b, energy_demand_b, delta_b, delta_check)

            #POLLUTION
            pollution_a = engine.pol_calc(fossil_prod_a)
            pollution_b = engine.pol_calc(fossil_prod_b)

            #TEMPERATURE
            temp_change_a = engine.temp_change(pollution_a)
            temp_change_b = engine.temp_change(pollution_b)

            temp_delta_a = engine.temp_delta(triangle_a['temp_delta'], temp_change_a)
            temp_delta_b = engine.temp_delta(triangle_b['temp_delta'], temp_change_b)

            temp_a = engine.temperature(triangle_a['temperature'], temp_change_a)
            temp_b = engine.temperature(triangle_b['temperature'], temp_change_b)

            #PSYCHOLOGICAL DISTANCE
            psy_distance_a = engine.psy_distance(temp_delta_a)
            psy_distance_b = engine.psy_distance(temp_delta_b)


            ############################
            #EMIGRATION
            pop_emig = engine.population_emigrating(energy_supply_a, energy_supply_b, pollution_a, population_a)

            popu_change_a = engine.population_change(population_a, pop_emig)
            popu_change_b = engine.population_change(pollution_b, pop_emig)

            #################################
            ###  C I T Y   V A C A N C Y  ###
            #################################

            city_empty_a = engine.city_empty(popu_change_a, pop_emig, triangle_a['population_default'])
            city_empty_b = engine.city_empty(popu_change_b, pop_emig, triangle_b['population_default'])

            ###############################
            ### T R A N S   V A L U E #####
            ###############################

            transition_a = engine.trans_value(triangle_a['transition_value'], psy_distance_a, city_empty_a)
            transition_b = engine.trans_value(triangle_b['transition_value'], psy_distance_b, city_empty_b)


            turn_in += 1
            start_turn(turn_in)



start_turn(1)