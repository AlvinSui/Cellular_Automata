# -*- coding: utf-8 -*-
"""
Created on Thu Jun 21 18:40:21 2018

@author: Nils
"""

GH_tr_nr = []
LU_abiotic = []
LU_agri = []
LU_green = []
LU_urban = []
LU_water = []
population = []
rain = []
temp = []
b = []


# LINK FILEPATH-NODE TO GENERAL DATABASE .CSV TO A READFILE-NODE AND THEN TO PHYTON-NODE (with this code) THROUGH 'GD'
for row in GD: 
    lines = row.split(',')
    b.append(lines)

# extracts one value and turns the columns into lists
for x in range(320):
    GH_tr_nr.append((b[x][1]))
    LU_abiotic.append((b[x][2]))
    LU_agri.append((b[x][3]))
    LU_green.append((b[x][4]))
    LU_urban.append((b[x][5]))
    LU_water.append((b[x][6]))
    population.append((b[x][7]))

#deletes all first items per list(the header)
del GH_tr_nr [0]
del LU_abiotic [0]
del LU_agri [0]
del LU_green [0]
del LU_urban [0]
del LU_water [0]
del population [0]

#turns all strings in list into floats
def floatinator (list):
    for val in list:
        float(val)
        return list

floatinator(GH_tr_nr)
floatinator(LU_abiotic)
floatinator(LU_agri)
floatinator(LU_green)
floatinator(LU_urban)
floatinator(LU_water)
floatinator(population)