#! /usr/bin/env python

class myCountry(object):
    def __init__(self, country_name, population, triangle_id):
        self.country     = country_name
        self.population  = population
        self.triangle_id = triangle_id

# make sure myGeo.py is in the same directory as this file
import myGeo as myg

# create points
pnt0 = myg.myPoint(0, 1, 2)
pnt1 = myg.myPoint(2, 1, 5)
pnt2 = myg.myPoint(-1, -1, -1)

# create a triangle
myTri = myg.myTriangle(pnt0, pnt1, pnt2, 0)
# calculate and print the coordinates of the triangle normal
normal = myTri.face_normal()
for i in range(3):
    print normal[i]

# create some countries
America = myCountry("America", 1, 0)
Liechtenstein = myCountry("Liechtenstein", 100000000, 1)
