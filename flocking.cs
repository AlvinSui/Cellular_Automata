private void RunScript(bool init, int subit, List<Point3d> startpt, int size, bool wrap, ref object A, ref object B)
  {
    //private void RunScript(double init, int subit, List<Point3d > startpt, object size, bool warp, ref object A, ref object B)
    //{
    List<Point3d> pts = new List<Point3d>();
    List<Vector3d> dir = new List<Vector3d>();

    if (init)
      setup(startpt, size, wrap);

    for (int i = 0; i < subit; i++)
    {
      flck.run();
    }

    foreach (Boid bo in flck.boids)
    {
      pts.Add(bo.loc);
      dir.Add(bo.vel);
    }

    A = pts;
    B = dir;

    // <Custom additional code>

    //
    //



  }

  // <Custom additional code> 
  //Flocking
  // by Daniel Shiffman.
  // Ported to Grasshopper (VB) by Vicente Soler (vicente@visose.com)
  // Translated to C# by Pirouz Nourian https://sites.google.com/site/pirouznourian
  //
  // An implementation of Craig Reynold's Boids program to simulate
  // the flocking behavior of birds. Each boid steers itself based on
  // rules of avoidance, alignment, and coherence.
  //
  private Flock flck = new Flock();
  public static Point3d max = new Point3d();
  public static bool wrp;

  public void setup(List < Point3d > startpt, int size, bool warp)
  {
    flck = new Flock();
    max = new Point3d(size, size, size);
    wrp = warp;
    Random rnd = new Random(1234);

    // Add an initial Set Of boids into the system
    for (int i = 0; i <= startpt.Count - 1; i++)
    {
      double velx = (rnd.NextDouble() * 2) - 1;
      double vely = (rnd.NextDouble() * 2) - 1;
      double velz = (rnd.NextDouble() * 2) - 1;
      Vector3d dir = new Vector3d(velx, vely, velz);
      flck.addBoid(new Boid(startpt[i], 3.0, 0.05, dir));
    }
  }

  // The Flock(a List Of Boid objects)
  public class Flock
  {
    public List<Boid> boids = new List<Boid>(); // An arraylist For all the boids

    public void run()
    {
      for (int i = 0; i <= boids.Count - 1; i++)
      {
        Boid bo = boids[i];
        bo.run(boids); // Passing the entire List Of boids To Each boid individually
      }
    }

    public void addBoid(Boid bo)
    {
      boids.Add(bo);
    }
  }

  // The Boid Class
  public class Boid
  {
    public Point3d loc = new Point3d();
    public Vector3d vel = new Vector3d();
    private Vector3d acc = new Vector3d();
    private double r = 4.2;
    private double maxforce;   // Maximum steering force
    private double maxspeed;   // Maximum speed

    public Boid(Point3d l, double ms, double mf, Vector3d v)
    {
      acc = new Vector3d(0, 0, 0);
      vel = v / (double) v.Length;
      loc = l;
      maxspeed = ms;
      maxforce = mf;
    }

    public void run(List <Boid> boids)
    {
      flock(boids);
      update();
      if (wrp)
        borders();
    }

    public void borders()
    {
      if ((loc.X < -r))
        loc.X = max.X + r;
      if ((loc.Y < -r))
        loc.Y = max.Y + r;
      if ((loc.Z < -r))
        loc.Z = max.Z + r;
      if ((loc.X > max.X + r))
        loc.X = -r;
      if ((loc.Y > max.Y + r))
        loc.Y = -r;
      if ((loc.Z > max.Z + r))
        loc.Z = -r;
    }

    // We accumulate a New acceleration Each time based On three rules
    public void flock(List<Boid> boids)
    {
      Vector3d sep = separate(boids); // Separation
      Vector3d ali = align(boids); // Alignment
      Vector3d coh = cohesion(boids); // Cohesion
      // Arbitrarily weight these forces
      sep *= 1.2;
      ali *= 1.2;
      coh *= 1.5;
      // Add the force vectors To acceleration
      acc += sep;
      acc += ali;
      acc += coh;
    }

    // Method To update location
    public void update()
    {
      // Update velocity
      vel += acc;
      // Limit speed
      if (vel.Length > maxspeed)
        vel = (vel / (double) vel.Length) * maxspeed;
      loc += vel;
      // Reset accelertion To 0 Each cycle
      acc = new Vector3d(0, 0, 0);
    }

    // A method that calculates a steering vector towards a target
    // Takes a second argument, If True, it slows down As it approaches the target
    private Vector3d steer(Point3d target, bool slowdown)
    {
      Vector3d SteerVec = new Vector3d(0, 0, 0);
      Vector3d desired = target - loc; // A vector pointing from the location To the target
      double d = desired.Length; // Distance from the target Is the magnitude Of the vector
      // If the distance Is greater than 0, calc steering(otherwise Return zero vector)
      if ((d > 0))
      {
        // Normalize desired
        desired.Unitize();
        // Two options For desired vector magnitude(1 - -based On distance, 2 -- maxspeed)
        if (((slowdown) &(d < 100.0)))
          desired *= maxspeed * (d / 100.0); // This damping Is somewhat arbitrary
        else
          desired *= maxspeed;

        // Steering = Desired minus Velocity
        SteerVec = desired - vel;
        // Limit To maximum steering force
        if (SteerVec.Length > maxforce)
          SteerVec = (1.0 / (double) SteerVec.Length) * maxforce * SteerVec;
      }
      else
        SteerVec = new Vector3d(0, 0, 0);
      return SteerVec;
    }


    // Separation
    // Method checks For nearby boids And steers away
    private Vector3d separate(List<Boid> boids)
    {
      double desiredseparation = 20.0;
      Vector3d steer = new Vector3d(0, 0, 0);
      int count = 0;
      // For every boid In the system, check If it's too close
      for (int i = 0; i <= boids.Count - 1; i++)
      {
        Boid other = boids[i];
        double d = loc.DistanceTo(other.loc);

        // If the distance Is greater than 0 And less than an arbitrary amount(0 When you are yourself)
        if ((d > 0) & (d < desiredseparation))
        {
          // Calculate vector pointing away from neighbor
          Vector3d diff = new Vector3d(loc - other.loc);
          diff.Unitize();
          diff /= d;      // Weight by distance
          steer += diff;
          count += 1;     // Keep track Of how many
        }
      }
      // Average -- divide by how many
      if ((count > 0))
        steer /= (double) count;

      // As Long As the vector Is greater than 0
      if ((steer.Length > 0))
      {
        // Implement Reynolds: Steering = Desired - Velocity
        steer.Unitize();
        steer *= maxspeed;
        steer -= vel;
        if (steer.Length > maxforce)
          steer = (steer / (double) steer.Length) * maxforce;
      }
      return steer;
    }

    // Alignment
    // For every nearby boid In the system, calculate the average velocity
    private Vector3d align(List <Boid> boids)
    {
      double neighbordist = 25.0;

      Vector3d steer = new Vector3d(0, 0, 0);
      int count = 0;
      for (int i = 0; i <= boids.Count - 1; i++)
      {
        Boid other = boids[i];
        double d = loc.DistanceTo(other.loc);

        if ((d > 0) & (d < neighbordist))
        {
          steer += other.vel;
          count += 1;
        }
      }
      if ((count > 0))
        steer /= (double) count;

      // As Long As the vector Is greater than 0
      if ((steer.Length > 0))
      {
        // Implement Reynolds: Steering = Desired - Velocity
        steer.Unitize();
        steer *= maxspeed;
        steer -= vel;
        if (steer.Length > maxforce)
          steer = (steer / (double) steer.Length) * maxforce;
      }
      return steer;
    }

    // Cohesion
    // For the average location(i.e.center) Of all nearby boids, calculate steering vector towards that location
    private Vector3d cohesion(List<Boid> boids)
    {
      double neighbordist = 25.0;
      Vector3d sum = new Vector3d(0, 0, 0); // Start With empty vector To accumulate all locations
      int count = 0;
      for (int i = 0; i <= boids.Count - 1; i++)
      {
        Boid other = boids[i];
        Point3d dista = new Point3d(loc.X, loc.Y, loc.Z);
        Point3d distb = new Point3d(other.loc.X, other.loc.Y, other.loc.Z);
        double d = distb.DistanceTo(dista);

        if (((d > 0) & (d < neighbordist)))
        {
          sum += new Vector3d(other.loc);// Add location
          count += 1;
        }
      }
      if ((count > 0))
      {
        sum /= (double) count;
        return steer(new Point3d(sum), false); // Steer towards the location
      }
      return sum;
    }
  }
  // </Custom additional code> 
}